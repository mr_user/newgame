package
{
	import flash.display.Bitmap;
	import flash.display.Stage;
	import flash.net.drm.AddToDeviceGroupSetting;
	import flash.utils.Dictionary;
	
	import graphics.HeroShip;
	import graphics.WelcomeScreenLayer;
	
	import starling.display.Image;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	

	public class Assets
	{
		[Embed(source="../SpaceRangers/fast99.ttf", embedAsCFF="false", fontFamily="Fast")]
		private static const FastFont:Class;
		
		//-----------------------------------------
		
		[Embed(source = "../SpaceRangers/background.png")]
		public static const Background: Class;
	
		[Embed(source = "../SpaceRangers/welcomescreen.png")]
		public static const WelcomeScreen: Class;
		
		//------------------------------------------
		
		[Embed(source="../SpaceRangers/mainAtlas.xml", mimeType="application/octet-stream")]
		public static const AtlasXml:Class;
		
		[Embed(source="../SpaceRangers/mainAtlas.png")]
		public static const AtlasTexture:Class;
		
		//------------------------------------------
		
		[Embed(source="../SpaceRangers/buttonAtlas.xml", mimeType="application/octet-stream")]
		public static const ButtonAtlasXml:Class;
		
		[Embed(source="../SpaceRangers/buttonAtlas.png")]
		public static const ButtonAtlasTexture:Class;

	}
}