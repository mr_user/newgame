package
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import starling.core.Starling;
	

	[SWF(width="800", height="600", frameRate="60", backgroundColor="#FFFFFF")]
	
	public class NewGame extends Sprite
	{
		
		
		//-------------Constants----------------
		
		
		
		//------------Constructor---------------
		
		private var myStarling:Starling;
		
		public function NewGame()
		{
			myStarling = new Starling(StarlingMain, stage);
			myStarling.start();
			trace ("starling was inited");
		}
		
		
		
	}
}