package
{
	import actions.Battle;
	
	import events.NavigationEvent;
	
	import graphics.BgLayer;
	import graphics.HeroShip;
	import graphics.Weapon;
	import graphics.WelcomeScreenLayer;
	
	import managers.GameManager;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.TouchEvent;
	
	
	
	public class StarlingMain extends Sprite
	{
		private var welcome:WelcomeScreenLayer;
		private var battle: Battle;
		private var bg:BgLayer;
		
		public static var current:Sprite;
		
		public function StarlingMain()
		{
			super();
			current = this;
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStageListener);
			bg = new BgLayer();
		}
		
		
		private function onAddedToStageListener(e:Event):void
		{
			trace('Starling loaded!');
			
			addEventListener(NavigationEvent.CHANGE_SCREEN, onChangeScreen);
			
			welcome = new WelcomeScreenLayer();
			
			addChild(welcome);
		}
		
		private function onChangeScreen(e:NavigationEvent):void
		{
			switch (e.params.id)
			{
				case "play":
					removeChild(welcome);
					addChild( bg );
					GameManager.startNewGame();
					break;
				case "help": 
					break;
			}
			
		}	
		
	
		
		}
		
}