package actions
{
	import flash.geom.Point;
	
	import graphics.Asteroids;
	import graphics.BgLayer;
	import graphics.HeroShip;
	import graphics.StatsLayer;
	import graphics.Weapon;
	
	import managers.GameManager;
	
	import starling.core.Starling;
	import starling.core.starling_internal;
	import starling.display.Sprite;
	import starling.display.Stage;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
		
	public class Battle extends Sprite
	{
		//-------------Constants----------------
		
	
		//------------Constructor---------------
		
		private var bglayer: BgLayer;
		private var stats: StatsLayer;
		
		public static var battleLayer:Sprite = new Sprite();
		private var hero: HeroShip;
		private var asteroid: Asteroids;
		
		private var localPos:Point;
		private var bullet:Weapon;
		private var bullets:Array = new Array();
		
		
		public function Battle()
		{
			super();
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage():void
		{
			trace ("Battle window initialized!");
	
			bglayer = new BgLayer;
			stats = new StatsLayer;
			hero = new HeroShip;
			
			hero.x = stage.stageWidth/2 - hero.width/2;
			hero.y = stage.stageHeight - hero.height - 30;
		
			addChild(bglayer);
			addChild(stats);
			
			
			addChild(battleLayer);
			
			GameManager.startNewGame();
		
			/*if (hero)	
			{
				hero.addEventListener(TouchEvent.TOUCH, heroMove);
				hero.addEventListener(TouchEvent.TOUCH, fire);
			}*/
		}	
		
		
		//---------------------------------------------------------
		//-------------- Event Listeners---------------------------
		//---------------------------------------------------------
		
		
		//---------------Start Firing------------------------------
		
		private function fire(e:TouchEvent):void
		{	
			var touch:Touch = e.getTouch(this, TouchPhase.BEGAN); 
			if (touch && GameManager.onFire == true)
			{
				bullet = new Weapon(hero.x + hero.width/2, hero.y);
				bullets.push(bullet);	
				addChild(bullet);
			}	
			
			Starling.current.stage.addEventListener(Event.ENTER_FRAME, onBulletFly);
		}	
		
		
		
		//--------------Bullet Flying--------------------------
		
		private function onBulletFly():void
		{
			for (var i:int = 0; i < bullets.length; i++) 
			{	
				var currBullet:Weapon = bullets[i];
				currBullet.y -= bullet.speed.y;	
				if (currBullet.y < 0)
				{
					removeChild( currBullet );
					bullets.splice(i,1);
				}
			}	
			trace (bullets.length);
		}		
		
		
		
		//---------Moving the ship------------------------------
		
		private function heroMove(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(this, TouchPhase.MOVED);
			
			if (touch)
			{
				localPos = touch.getLocation(this);
				
				hero.x = localPos.x;
				hero.y = localPos.y;
				
				if (hero.x >= stage.stageWidth - hero.width)
					hero.x = stage.stageWidth - hero.width;
				else if
					(hero.x <=0)
					hero.x = 0;
				else if
					(hero.y >= stage.stageHeight - hero.height)
					hero.y = stage.stageHeight - hero.height;
				else if
					(hero.y <=0)
					hero.y = 0;
			}
		}
		
		
	}	
}