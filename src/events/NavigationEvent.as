package events
{
	import starling.events.Event;
	
	public class NavigationEvent extends Event
	{
		
		
		//-------------Constants----------------
		
		public static const CHANGE_SCREEN:String = "changeScreen";
		public var params:Object;
		
		//------------Constructor---------------
		
		
		public function NavigationEvent(type:String, _params:Object, bubbles:Boolean)
		{
			super(type, bubbles);
			params = _params;
		}
		
		
		
		
		//-------------Variables----------------
		
		
		
		//-------Overriden properties-----------
		
		
		
		//------------Properties----------------
		
		
		
		//---------Overriden methods------------
		
		
		
		//--------------Methods-----------------
		
		
		
		//--------------Handlers----------------
		
		
	}
}