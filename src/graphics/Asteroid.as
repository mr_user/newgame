package graphics
{
	import flash.display3D.IndexBuffer3D;
	import flash.geom.Point;
	
	import starling.display.Image;
	import starling.textures.Texture;

	public class Asteroid extends Image implements IBattleObject
	{
		//---------------------------------------------------------------
		//
		// Variables
		//
		//---------------------------------------------------------------
		
		
		//---------------------------------------------------------------
		//
		// Components
		//
		//---------------------------------------------------------------
		
		public function get speed():Point
		{
			return new Point(0, 5);
		}
		
		public function get pos():Point
		{
			return new Point(x,y);
		}
		
		public function set pos(value:Point):void
		{
			x = value.x;
			y = value.y;
		}
		
		public function get radius():int
		{
			return width * 0.5;
		}
		
		//---------------------------------------------------------------
		//
		// Construct
		//
		//---------------------------------------------------------------
		
		public function Asteroid(texture:Texture)
		{
			super(texture);
		}
		
		//---------------------------------------------------------------
		//
		// Visual
		//
		//---------------------------------------------------------------
		
		//---------------------------------------------------------------
		//
		// Logic
		//
		//---------------------------------------------------------------
		
		//---------------------------------------------------------------
		//
		// Handlers
		//
		//---------------------------------------------------------------
		
	

	}
}