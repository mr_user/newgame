package graphics
{
	import flash.utils.Dictionary;
	
	import managers.TextureManager;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class Asteroids extends Sprite
	{
		
		
		//-------------Constants----------------
		
		//------------Constructor---------------
		
		
		private var asteroidsArr:Array; 
		private var ast:Image;
		
		public function Asteroids()
		{
			super();
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			addEventListener(Event.ENTER_FRAME, startFalling);
		}
		

		
		private function onAddedToStage():void
		{
			trace ("asteroids screen added!");

			asteroidsArr = new Array("asteroid01","asteroid02","asteroid03","asteroid04","asteroid05");
			
			var i:int = Math.random() * asteroidsArr.length;
			
			ast = new Image(TextureManager.getGameTexture("asteroids", asteroidsArr[i]));
			ast.rotation = Math.random()* Math.PI * 2;
			
			ast.x = Math.random() * (stage.stageWidth - ast.width);
			ast.y = -60;
			
			addChild(ast);
		}		
		
		private function startFalling():void
		{
			
		}			

		
		
		
		//-------------Variables----------------
		
		
		
		//-------Overriden properties-----------
		
		
		
		//------------Properties----------------
		
		
		
		//---------Overriden methods------------
		
		
		
		//--------------Methods-----------------
		
		
		
		//--------------Handlers----------------
		
		
	}
}