package graphics
{
	import starling.display.Image;
	import starling.display.Shape;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	
	public class BgLayer extends Sprite
	{
		
		
		//-------------Constants----------------
		
		//------------Constructor---------------
		
		private var starsArr:Array;
		private var speed:Number;
		private var star:Shape;
		
		public function BgLayer()
		{
			super();
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage():void
		{	
			trace ("backgroung initialized!");
			
			var img:Image = new Image(Texture.fromBitmap(new Assets.Background));
			addChild(img);
			
			createStars();
			addEventListener(Event.ENTER_FRAME, onMove);
		}
		
	
		private function createStars():void
		{
			starsArr = new Array();
			
			for (var i:int = 0; i < 200; i++) 	
			{	
				star = new Shape();
				
				var xPos:Number = Math.random()*stage.stageWidth;
				var yPos:Number = Math.random()*stage.stageHeight;
				var radius:Number = Math.random()*1.5;
				
				star.graphics.beginFill(0xffffff);
				star.graphics.drawCircle(0,0,radius);	
				star.graphics.endFill();
				star.alpha = Math.random();
				
				star.x = xPos;
				star.y = yPos;
				
				addChild(star);
				starsArr.push(star);	
			}
		
		}	
		
		private function onMove():void
		{
			var stH:int = stage.stageHeight
			for each (var star:Shape in starsArr) 
			{
				speed = Math.random()*3;
				star.y +=speed;
				
				
				
				if (star.y >= stH)
					star.y = 0;
			}
		
		}		

		//-------------Variables----------------
		
		
		
		//-------Overriden properties-----------
		
		
		
		//------------Properties----------------
		
		
		
		//---------Overriden methods------------
		
		
		
		//--------------Methods-----------------
		
		
		
		//--------------Handlers----------------
		
		
	}
}