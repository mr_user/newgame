package graphics
{
	import actions.Battle;
	
	import flash.geom.Point;
	import flash.utils.setTimeout;
	
	import graphics.Weapon;
	
	import managers.BattleObjects;
	import managers.TextureManager;
	
	import starling.animation.DelayedCall;
	import starling.core.Starling;
	import starling.core.starling_internal;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	public class HeroShip extends Sprite implements IBattleObject
	{
		public function get speed():Point
		{
			return new Point(0, 0);
		}
		
		public function get pos():Point
		{
			return new Point(x,y);
		}
		
		public function set pos(value:Point):void
		{
			x = value.x;
			y = value.y;
		}
		
		public function get radius():int
		{
			return width * 0.5;
		}
		
		public static var bullet:Weapon;
		
		public function HeroShip()
		{
			super();
			
			var texture:Texture = Texture.fromBitmap(new Assets.AtlasTexture());
			var xml:XML = XML(new Assets.AtlasXml());
			var atlas:TextureAtlas = new TextureAtlas(texture, xml);
			
			var fligth:MovieClip = new MovieClip(atlas.getTextures("heroship_"), 15);
			fligth.loop = true; 
			addChild(fligth);
			fligth.play();
			
			Starling.juggler.add(fligth);	
			alignPivot();
			
			addEventListener(TouchEvent.TOUCH, heroMove);
			
			x = Starling.current.stage.stageWidth/2 - width/2;
			y = Starling.current.stage.stageHeight - height - 30;
		}
	
		
		private var _isLocked:Boolean = false;
		
	
		
		private function heroMove(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(this, TouchPhase.MOVED);
			
			if (touch)
			{
				var newPosX:int = touch.globalX;
				var hasMovedLeft:Boolean = x > newPosX;
				var hasMovedRight:Boolean = x < newPosX;
				
				x = newPosX;
				y = touch.globalY;
				
				if (!_isLocked)
				{
					BattleObjects.createBullet();
					_isLocked = true;
					setTimeout( function ():void
					{
						_isLocked = false;
					}, 20);
				}
			}
			else
			{
				
			}
		}
		
	
	}
}