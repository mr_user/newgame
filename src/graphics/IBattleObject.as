package graphics
{
	import flash.geom.Point;

	public interface IBattleObject
	{
		function get speed():Point

		function get pos():Point
		function set pos(value:Point):void
		
		function get radius():int
	
	}
}