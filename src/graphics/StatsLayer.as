package graphics
{
	import managers.GameManager;
	
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	
	public class StatsLayer extends Sprite
	{
		
		
		//-------------Constants----------------
		
		
		
		//------------Constructor---------------
		
		
		public function StatsLayer()
		{
			super();
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage():void
		{
			trace("stats screen initialized!");
			
			var scoreTf1:TextField = new TextField(120, 30, "Your Score:", "Fast");
			scoreTf1.color = 0xffffff;
			scoreTf1.fontSize = 18;
			//scoreTf1.border = true;
			scoreTf1.x = stage.stageWidth - scoreTf1.width;
			addChild(scoreTf1);
			
			var scoreTf2:TextField = new TextField(100, 30, GameManager.score.toString(), "Fast");
			scoreTf2.color = 0xffffff;
			scoreTf2.fontSize = 20;
			//scoreTf2.border = true;
			scoreTf2.x = scoreTf1.x + (scoreTf1.width - scoreTf2.width)/2;
			scoreTf2.y = scoreTf1.y + scoreTf1.height;
			addChild(scoreTf2);
		}		
		
		
		
		
		//-------------Variables----------------
		
		
		
		//-------Overriden properties-----------
		
		
		
		//------------Properties----------------
		
		
		
		//---------Overriden methods------------
		
		
		
		//--------------Methods-----------------
		
		
		
		//--------------Handlers----------------
		
		
	}
}