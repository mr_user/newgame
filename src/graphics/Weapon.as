package graphics
{
	import flash.geom.Point;
	
	import managers.GameManager;
	import managers.TextureManager;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	public class Weapon extends Sprite implements IBattleObject
	{

		public function get speed():Point
		{
			return new Point(0, -5);
		}
		
		public function get pos():Point
		{
			return new Point(x,y);
		}
		
		public function set pos(value:Point):void
		{
			x = value.x;
			y = value.y;
		}
		
		public function get radius():int
		{
			return 1;
		}
		
		private var img:Image;
		
		public function Weapon(posX:Number, posY:Number)
		{
			super();
			img = new Image(TextureManager.getGameTexture("weapon", GameManager.currWeapon));
			addChild(img);
			
			x = posX;
			y = posY;
			
			alignPivot();
		}

	}
	
}