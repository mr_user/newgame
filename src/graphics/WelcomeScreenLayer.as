package graphics
{
	import events.NavigationEvent;
	
	import managers.TextureManager;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.display.Stage;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	public class WelcomeScreenLayer extends Sprite
	{
		
		
		//-------------Constants----------------
		
		//------------Constructor---------------
		
		private var playbtn: Button;
		private var helpbtn: Button;
		
		public function WelcomeScreenLayer()
		{
			super();
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage():void
		{
			trace("welcome screen initialized!");
			
			var welcomeScreen: Image = Image.fromBitmap(new Assets.WelcomeScreen());
			this.addChild(welcomeScreen);
			
			createButtons();
		}		
		
		private function createButtons():void
		{
			playbtn = new Button(TextureManager.getGameTexture("button", "btn2"), "start game");
			helpbtn = new Button(TextureManager.getGameTexture("button", "btn2"), "help");
			
			playbtn.fontColor = 0xffffff;
			helpbtn.fontColor = 0xffffff;
			
			playbtn.fontSize = 20;
			helpbtn.fontSize = 20;
			
			helpbtn.x = stage.stageWidth/2 - helpbtn.width/2;
			helpbtn.y = stage.stageHeight - helpbtn.height - 30;
			
			playbtn.x = helpbtn.x;
			playbtn.y = helpbtn.y - playbtn.height - 10;
			
			this.addChild(playbtn);
			this.addChild(helpbtn);
			
			this.addEventListener(Event.TRIGGERED, onClick);
		}
		
		private function onClick(e:Event):void
		{
			var clicked: Button = e.target as Button;
			
			if ((clicked as Button) == playbtn)
			{	
				dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: "play"}, true));
			}
			
			else if ((clicked as Button) == helpbtn)
			{
				dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: "help"}, true));
			}
			
		}	
	
		//-------------Variables----------------
		
		
		
		//-------Overriden properties-----------
		
		
		
		//------------Properties----------------
		
		
		
		//---------Overriden methods------------
		
		
		
		//--------------Methods-----------------
		
		
		
		//--------------Handlers----------------
		
	}
}