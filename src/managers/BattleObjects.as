package managers
{
	import graphics.Asteroid;
	import graphics.Asteroids;
	import graphics.HeroShip;
	import graphics.Weapon;
	
	import starling.core.Starling;
	import starling.display.Sprite;

	public class BattleObjects
	{
		//---------------------------------------------------------------
		//
		// Variables
		//
		//---------------------------------------------------------------
		
		public static var hero:HeroShip;
		
		public static var bullets:Vector.<Weapon>;
		
		public static var asteroids:Vector.<Asteroid>;
		
		public static var _mainLayer:Sprite;
		
		//---------------------------------------------------------------
		//
		// Components
		//
		//---------------------------------------------------------------
		
		//---------------------------------------------------------------
		//
		// Construct
		//
		//---------------------------------------------------------------
		
		public function BattleObjects()
		{
		}
		
		//---------------------------------------------------------------
		//
		// Visual
		//
		//---------------------------------------------------------------
		
		private static const asteroidsArr:Array = new Array("asteroid01","asteroid02","asteroid03","asteroid04","asteroid05");
		
		//---------------------------------------------------------------
		//
		// Logic
		//
		//---------------------------------------------------------------
		
		public static function createAsteroid():Asteroid
		{
			var i:int = Math.random() * asteroidsArr.length;
			
			var ast:Asteroid = new Asteroid(TextureManager.getGameTexture("asteroids", asteroidsArr[i]));
			ast.rotation = Math.random()* Math.PI * 2;
			
			ast.x = Math.random() * (Starling.current.stage.stageWidth - ast.width);
			ast.y = -60;
			
			asteroids.push(ast);
			_mainLayer.addChild(ast);
			
			return ast;
		}
		
		public static function createBullet():Weapon
		{
			var bullet:Weapon = new Weapon(hero.x, hero.y);
			bullets.push(bullet);	
			_mainLayer.addChild(bullet);
			return bullet;
		}
		
		public static function createHero():HeroShip
		{
			hero = new HeroShip();
			_mainLayer.addChild(hero);
			return hero;
		}
		
		public static function removeAsteroid(currObj:Asteroid, index:int):void
		{
			_mainLayer.removeChild( currObj );
			asteroids.splice( index,1);
		}
		
		//---------------------------------------------------------------
		//
		// Handlers
		//
		//---------------------------------------------------------------
		
		
		public static function removeWeapon(currBul:Weapon, index:int):void
		{
			_mainLayer.removeChild( currBul );
			bullets.splice( index,1);
		}
	}
}