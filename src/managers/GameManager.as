package managers
{
	import graphics.Asteroid;
	import graphics.HeroShip;
	import graphics.IBattleObject;
	import graphics.Weapon;
	
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.Event;

	public class GameManager
	{
		
		
		//-------------Constants----------------
		
		
		
		//------------Constructor---------------
		private static var _currWeapon:String = "weapon01";
		
		public static var onFire:Boolean = true;
		
		private static var _score:int = 0;
		
		private static var _currLevel:int = 1;
		
		public function GameManager()
		{
		}
		
		
		
		
		//-------------Variables----------------
		
		
		
		//-------Overriden properties-----------
		
		
		
		//------------Properties----------------
		
		
		public static function get currLevel():int
		{
			return _currLevel;
		}

		public static function set currLevel(value:int):void
		{
			_currLevel = value;
		}

		public static function get score():int
		{
			return _score;
		}

		public static function set score(value:int):void
		{
			_score = value;
		}

		public static function get currWeapon():String
		{
			return _currWeapon;
		}
		
		public static function set currWeapon(value:String):void
		{
			_currWeapon = value;
		}
		
		
		//---------Overriden methods------------
		
		
		
		//--------------Methods-----------------
		
		public static function startNewGame():void
		{
			
			BattleObjects.asteroids = new Vector.<Asteroid>();
			BattleObjects.bullets = new Vector.<Weapon>();
			BattleObjects._mainLayer = StarlingMain.current;
			
			BattleObjects.hero = BattleObjects.createHero();
			
			for (var i:int = 0; i < 10; i++) 
				BattleObjects.createAsteroid();
			
			StarlingMain.current.stage.addEventListener(Event.ENTER_FRAME, StepProcessor);
		}
		
		private static function StepProcessor(e:Event):void
		{
			var currAst:Asteroid;
			var currBul:Weapon;
			
			var i:int;
			
			var starlingWidth:int = Starling.current.stage.stageWidth;
			for (i = 0; i < BattleObjects.asteroids.length; i++) 
			{
				if (i>= BattleObjects.asteroids.length)
					break;
				
				currAst = BattleObjects.asteroids[i];
				moveObj(currAst);
				if (currAst.pos.y > starlingWidth + currAst.radius)
					BattleObjects.removeAsteroid(currAst, i);
			}
			
			for (i = 0; i < BattleObjects.bullets.length; i++) 
			{
				if (i>= BattleObjects.bullets.length)
					break;
				
				currBul = BattleObjects.bullets[i];
				moveObj(currBul);
				if (currBul.pos.y < - currBul.radius)
					BattleObjects.removeWeapon(currBul, i);
			}
			
		}		
		
		private static function moveObj(currObj:IBattleObject):void
		{
			currObj.pos = currObj.pos.add(currObj.speed);
		}
		
		//--------------Handlers----------------
		
		
		
	}
}