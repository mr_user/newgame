package managers
{
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;

	public class TextureManager
	{
		
		
		//-------------Constants----------------
		
		
		
		//------------Constructor---------------
		
		
		public function TextureManager()
		{
		}
		

		public static function getGameTexture(textureType:String, subTextureName:String):Texture
		{
			var texture:Texture;
			var xml:XML;
			
			switch (textureType)
			{
				case "button":
					texture = Texture.fromBitmap(new Assets.ButtonAtlasTexture());	
					xml = XML(new Assets.ButtonAtlasXml());
				break;
				
				case "weapon":
					texture = Texture.fromBitmap(new Assets.AtlasTexture());
					xml = XML(new Assets.AtlasXml());
				break;
				
				case "asteroids":
					texture = Texture.fromBitmap(new Assets.AtlasTexture());
					xml = XML(new Assets.AtlasXml());
				break;
			}
			
			var atlas:TextureAtlas = new TextureAtlas(texture, xml);
			var subtexture: Texture = atlas.getTexture(subTextureName);
			
			return subtexture;
		}
		
		
		//-------------Variables----------------
		
		
		
		//-------Overriden properties-----------
		
		
		
		//------------Properties----------------
		
		
		
		//---------Overriden methods------------
		
		
		
		//--------------Methods-----------------
		
		
		
		//--------------Handlers----------------
		
		
	}
}